
#####################
#LIBRERIAS NECESARIAS
# netifaces         'pip install netifaces'
# numpy             'pip install numpy'
# matplotlib        'pip install matplotlib'
#####################

import xmlrpc.server
from xmlrpc.client import Binary
import netifaces
from math import sin, cos, tan, sqrt
from matplotlib import pyplot
from time import time
from numpy import linspace
from os import makedirs
from os.path import exists, dirname


def get_ip_addresses():
    ip = list()
    for addr in netifaces.interfaces():
        try:
            ip.append(netifaces.ifaddresses(addr)[2][0]["addr"])
        except:
            pass
    return ip


def format_string(string):
    string = string.replace(" ", "")
    string = string.replace("sen(", "sin(")
    return string


def do_func(f, x):
    f = f.replace("x", str(x))
    try:
        fx = eval(f)
    except:
        fx = None
    print(f)
    print(fx)
    return fx
    

def resolver_o_graficar(string, dimentions):
    # print(dimentions)
    maxX, maxY, minX, minY = dimentions
    #print(dimentions)
    print("Calculando...")
    if string.lower().startswith("f(x)="):
        
        string = string.split("=")[1]
        x = linspace(-10,10, 1000)

        pyplot.plot(x, [do_func(string, i) for i in x])
        pyplot.axhline(0, color="black")
        pyplot.axvline(0, color="black")
        pyplot.xlim(minX, maxX)
        pyplot.ylim(minY, maxY)
        t = time()
        imgPath = f"graphs\\{t}.png"
        if not exists(dirname(imgPath)):
            makedirs(dirname(imgPath))
        pyplot.savefig(imgPath)
        pyplot.clf()


        with open(imgPath, "rb") as f:
            content = f.read()

        print("Graficacion completada, enviando al cliente")
        return Binary(content)
    else:
        string = eval(string)
        print("Aritmetica completada, enviando al cliente")
        return string


def realizar_operacion(op:str, graphDimentions:tuple):

    op = format_string(op)
    op = resolver_o_graficar(op, graphDimentions)
    return op

def check_conexion():
    return True



if __name__ == "__main__":
    with xmlrpc.server.SimpleXMLRPCServer(('0.0.0.0', 8000)) as server:
        server.register_function(realizar_operacion)
        server.register_function(check_conexion)

        ip = get_ip_addresses()
        print(f"Servidor en linea en {ip}")
        # Inicie el servidor
        server.serve_forever()
