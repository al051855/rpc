
#####################
#LIBRERIAS NECESARIAS
# PyQt5             'pip install PyQt5'
#####################

from PyQt5 import QtWidgets, QtGui
from gui.GUI_rpc import Ui_CalculadoraRPC
import xmlrpc.client


# serverIp = "10.0.0.4"
serverPort = "8000"

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_CalculadoraRPC()
        self.ui.setupUi(self)

        self.ui.btnResolver.clicked.connect(self.enviar_operacion)
        self.ui.btnConectar.clicked.connect(self.realizar_conexion)

        self.ui.lblGraph.setScaledContents(True)
        self.ui.lblGraph.setSizePolicy(QtWidgets.QSizePolicy.Ignored, QtWidgets.QSizePolicy.Ignored)

    def realizar_conexion(self):
        serverIp = self.ui.lineEditIP.text()
        self.proxy = xmlrpc.client.ServerProxy(f"http://{serverIp}:{serverPort}")
        try:
            conectado = self.proxy.check_conexion()
            if conectado:
                QtWidgets.QMessageBox.information(self, "Conectado exitosamente", f"Conectado al servidor {serverIp}")
            else:
                QtWidgets.QMessageBox.critical(self, "Error de conexion", f"No se a podido establecer comunicacion con el servidor {serverIp}")
                
        except Exception as err:
            QtWidgets.QMessageBox.critical(self, "Error de conexion", f"No se a podido establecer comunicacion con el servidor {serverIp}\n{err}")
            print(err)
        

    def enviar_operacion(self):
        op = self.ui.lineEditOperacion.text()
        graphD = self.ui.dSBMaxX.value(), self.ui.dSBMaxY.value(), self.ui.dSBMinX.value(), self.ui.dSBMinY.value()
        print(f"Enviando '{op},{graphD}' al servidor")
        try:
            respuesta = self.proxy.realizar_operacion(op, graphD)
            if (isinstance(respuesta, xmlrpc.client.Binary)):
                pixmap = QtGui.QPixmap()
                pixmap.loadFromData(respuesta.data)
                self.ui.lblGraph.setPixmap(pixmap)
                self.ui.lblRespuesta.setText("")
                print("Se a recibido una grafica como respuesta")
            else:
                self.ui.lblRespuesta.setText(str(respuesta))
                print("Se a recibido un numero como respuesta")
        except xmlrpc.client.Fault as e:
            respuesta = F"ERROR '{op}' NO SE A PODIDO EJECUTAR EN EL SERVIDOR\n{e}"


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())
